# Semantic Versioning Changelog

## [1.0.1](https://gitlab.com/pss-x/support/gitlab-ci-templates/releases/compare/v1.0.0...v1.0.1) (2025-02-26)


### Bug Fixes

* rebranding applied [skip ci] ([c3e8aae](https://gitlab.com/pss-x/support/gitlab-ci-templates/releases/commit/c3e8aae9a36b1b647354746afedbab2a6f8daebd))
* renovate json updated [skip ci] ([b2f9bf9](https://gitlab.com/pss-x/support/gitlab-ci-templates/releases/commit/b2f9bf93fc7effb86ed23053bfffa1d4cca3d455))
* renovate json updated [skip ci] ([95e50c6](https://gitlab.com/pss-x/support/gitlab-ci-templates/releases/commit/95e50c6515d14d8832d1ca69fc0a603f7dc3695a))

# 1.0.0 (2025-02-12)


### Bug Fixes

* outdated sha256 changed ([42b22e5](https://gitlab.com/pss-x/support/gitlab-ci-templates/releases/commit/42b22e54817833fdd9225bd320515908421da28d))
