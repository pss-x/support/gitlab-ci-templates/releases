# Fetch Version and Release

Semantic-release is a tool designed to automate continuous delivery, as such:

* it determines the release number automatically
* it assumes that each commit pushed or merged to a release branch is ready to be release and therefore expect you to have a robust test suite to ensure that
* release each commit pushed or merged to a release branch right away

Semantic-release is compatible with git workflow like [Trunk Based Development](https://trunkbaseddevelopment.com/) or [GitHub Flow](https://docs.github.com/en/get-started/quickstart/github-flow).

On the other hand [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/) is the exact opposite, it let you manage the content and the timing of your release manually via release branches. 
It's a very different way of working that has it own set of advantages and inconvenient, just like any other.

## Referencing CI Template

[Gitlab CI includes](https://docs.gitlab.com/ee/ci/yaml/#include) are used for a seamless pipeline intgration.

Following code snippet should be inserted on top of the `.gitlab-ci.yml` file for global usage:

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/releases'
    ref: v1.0.1 # select desired version here
    file: '/templates/changelog.and.releases.yml'
    inputs:
      version-stage: 'version'
      release-stage: 'release'
```

RECOMMENDED: use stable version like tag as `ref`.

## Configuration file

**semantic-release**’s options mode and plugins can be set via either:

- A `.releaserc.json` file, written in JSON or `.releaserc.yaml` file written in markup language. Examples are available at [config](./.config) folder.

## Pipeline stages

Stages needed to run all pipeline jobs in correct order

```yaml
stages:
  - version
  - release
```

## Getting started

1. Generate a Token with "api" scope via [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) or [Group Access Token](https://gitlab.com/groups/pssx/-/settings/access_tokens)

2. Set Environmental Variable in your Project/Group (Settings -> CI/CD / Variables)

   Variable Name **GL_TOKEN** or **GITLAB_TOKEN** and your access token as value

3. Use semantic commits and get change logs & tags **automatically**

4. Commit message can be linted by [commitlint](https://gitlab.com/pss-x/support/gitlab-ci-templates/commitlint) before **version** stage manually

5. (Optional) Every job after the version version will have a variable `${VERSION}` available with the latest version to use the for Build you images/binarys/...

By adding the version job to the relevant needs: section you can specifically expose the ${VERSION} to certain jobs, e.g.:

```yml
...
stages:
  - version
  - push:cr
  - release

push:cr:
  extends: .cr:build-container-image
  stage: push:cr
  needs: [ "version" ]
  variables:
    IMAGE_TAG: $VERSION
...	
```

6. (Optional) `${DEFAULT_RELEASERC}` value can be overriden by adding `.releaserc.json` to gitlab project root folder.

7. (Optional) CLI arguments take precedence over options configured in the configuration file via `SEMANTIC_RELEASE_EXTRA_ARGS` variable

## How does it work?

### Commit message format

**semantic-release** uses the commit messages to determine the consumer impact of changes in the codebase.
Following formalized conventions for commit messages, **semantic-release** automatically determines the next [semantic version](https://semver.org) number, generates a changelog and publishes the release.

By default, **semantic-release** uses [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format).
The commit message format can be changed with the `preset` or `config` options of the [@semantic-release/commit-analyzer](https://github.com/semantic-release/commit-analyzer#options) and [@semantic-release/release-notes-generator](https://github.com/semantic-release/release-notes-generator#options) plugins.

Tools such as [commitlint](https://gitlab.com/pss-x/support/gitlab-ci-templates/commitlint) can be used to help contributors and enforce valid commit messages.

You can enforce conventions for commit messages via [git hooks](https://gitlab.com/pss-x/support/semantic-commit-hook#installation)

The table below shows which commit message gets you which release type when `semantic-release` runs (using the default configuration):

| Commit message                                                                                                                                                                                   | Release type                                                                                                    |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | ~~Patch~~ Fix Release                                                                                           |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release                                                                                       |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release <br /> (Note that the `BREAKING CHANGE: ` token must be in the footer of the commit) |

### Gitlab Secret Variables

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | GL_TOKEN or GITLAB_TOKEN | x | A GitLab [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) or [group access token.](https://gitlab.com/groups/pssx/-/settings/access_tokens) |

## Changelog and Release Template

The `changelog.and.releases.yml` template automates next version calculation and commint linting 

It generates a default `.releaserc.json` configuration for the job unless it finds an existing one in the project root.

The default configuration will use `CI_COMMIT_REF_NAME` for branch name and parse commits according to [Angular commit message conventions](https://github.com/semantic-release/semantic-release#commit-message-format) - so it is best used with `commitlint` checks to ensure conventional commits are followed.

It will `dry run` to calculate next version and then creates `artifact` and `dotenv` value.

It requires the following:

* A `GL_TOKEN` or `GITLAB_TOKEN` protected masked variable with a project token (if internal group resources are needed in CI).
* A `version` stage defined in `.gitlab-ci.yml`.

A minimal pipeline might look like this:

```yml
include:
  - project: 'pss-x/support/gitlab-ci-templates/releases'
    ref: v1.0.1 # select desired version here
    file: '/templates/changelog.and.releases.yml'

stages:
  - version
  - push:cr
  - release  

push:cr:
  extends: .cr:build-container-image
  stage: push:cr
  needs: [ "version" ]
  variables:
    IMAGE_TAG: $VERSION
```

You can also override default job rules if you'd like to run it manually, in schedules, or with different branch patterns:

> [branch patterns](https://semantic-release.gitbook.io/semantic-release/usage/configuration#branches).

#### Can I set the initial release version of my package to 0.0.1?

This is not supported by semantic-release. [Semantic Versioning](https://semver.org/) rules apply differently to major version zero and supporting those differences is out of scope and not one of the goals of the semantic-release project.

If your project is under heavy development, with frequent breaking changes, and is not production ready yet we recommend [publishing pre-releases](https://semantic-release.gitbook.io/semantic-release/recipes/release-workflow/pre-releases#publishing-pre-releases).

### Troubleshooting

#### Rewrite commit message

It is possible to change the message or content of a commit if the original one does not match the required structure, or you want to change it for other reasons.

Depending on whether you want to change only the most recent commit or one or more older commits, and whether these commits are only available locally or have already been pushed, you have different options.

1. In GitLab, check how far back in the commit history you need to go:

 - If you already have a merge request open for your branch, you can check the Commits tab and use the total number of commits.
 - If you are working from a branch only:
   - Go to Code > Commits.
   - Select the dropdown list in the top left and find your branch.
   - Find the oldest commit you want to update, and count how far back that commit is.

To change the message of older or multiple commit messages, you need to use this command:
```bash
git rebase -i HEAD~n
```
where n is the number of commits that should be displayed.

```bash
git rebase -i HEAD~1
```
Please note that one or more text files will be opened.

2. Type `i` to enter INSERT mode, and then start editing the text.

The pick command tells Git to use the commits without change. 
You must change the command from `pick` to `reword` for the commits you want to update.

3. Save edited text. Press `Escape` to exit INSERT mode, then type `:wq` and Enter to save and exit.

4. Change message, highlighted with yellow. Press `Escape` to exit INSERT mode, then type `:wq` and Enter to save and exit.

5. If a commit needs to be replaced remotely, a ```git push --force branch_name``` is necessary.

```bash
git push --force branch-name
```

For a detailed description, please see [changing a commit message](https://docs.gitlab.com/ee/tutorials/update_commit_messages/)

A good description that shows how to use amend, reword, delete, reorder, squash and split commits can be found here:
* [Rewriting Git History (Blog Post)](https://www.themoderncoder.com/rewriting-git-history/)
* [Rewriting Git History (Video)](https://youtu.be/ElRzTuYln0M)

## Related documents

* [Fully automated version management and package publishing](https://github.com/semantic-release/semantic-release/tree/master#fully-automated-version-management-and-package-publishing)
* [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)
* [Changing a commit message](https://docs.gitlab.com/ee/tutorials/update_commit_messages/)
* [Rewriting Git History (Blog Post)](https://www.themoderncoder.com/rewriting-git-history/)
* [Rewriting Git History (Video)](https://youtu.be/ElRzTuYln0M)

## Have a Problem or Suggestion ?

Please see a list of features and bugs under issues If you have a problem which is not listed there, please let us known.